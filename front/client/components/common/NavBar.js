import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconBack from 'material-ui/svg-icons/navigation/chevron-left';
import SearchField from 'material-ui/TextField';
import {Link} from 'react-router-dom';


const BackButton = () => (
    <div>
        <Link to="/">
            <IconButton>
                <IconBack/>
            </IconButton>
        </Link>
    </div>
);

class NavBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            keyword: localStorage.keyword
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleSubmit(e) {
        if(e.keyCode === 13) {
            this.props.cb(this.state.keyword);
            localStorage.keyword = this.state.keyword;
        }
    }

    render() {
        return (
            <AppBar
                iconElementLeft={<BackButton/>}
                style={
                    {
                        background: 'white'
                    }
                }
                title={
                    <SearchField
                        value={this.state.keyword}
                        name="keyword"
                        type="search"
                        hintText="검색어를 입력해주세요"
                        style={{fontFamily: 'main'}}
                        onChange={this._handleChange}
                        onKeyUp={this._handleSubmit}
                    />
                }
                titleStyle={{color: 'black', textAlign: 'center', fontFamily: 'main', lineHeight: '70px', paddingRight: '2.5rem'}}
            />
        );
    }
}


export default NavBar;