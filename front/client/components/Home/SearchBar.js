import React from 'react';
import Paper from 'material-ui/Paper';
import SearchField from 'material-ui/TextField';
import {Link} from 'react-router-dom';
import Button from 'material-ui/RaisedButton';
import axios from 'axios';


class SearchBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            keyword: ''
        };

        this._handleChange = this._handleChange.bind(this);
        this._getLocation = this._getLocation.bind(this);
    }

    _handleChange(e) {
        if (e.keyCode === 13) {
            console.log('enter');
        }
        else {
            this.setState({
                [e.target.name]: e.target.value
            });
        }
    }

    _handleEnter(e) {
        if (e.charCode === 13)
            document.getElementById('submitButton').click();
    }

    _getLocation() {
        if (navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (location) {
                axios({
                    method: 'get',
                    baseURL: encodeURI(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${location.coords.latitude},${location.coords.longitude}&language=ko&key=AIzaSyA4zZrnJA3ak88ddBXwbtYHmlIGYWT_-ik`)
                }).then(function (response) {
                    var isDaegu = response.data.results[0].formatted_address.split(' ').indexOf('대구광역시');

                    if (isDaegu !== -1) {
                        this.setState({
                            keyword: response.data.results[0].formatted_address
                        });
                    }
                    else {
                        console.log('대구가 아닙니다');
                    }
                }.bind(this)).catch(function (err) {
                    console.error(err);
                })
            }.bind(this));
        }
        else {
            alert('wrong');
        }
    }

    componentDidMount() {
        this._getLocation();
    }

    render() {
        return (
            <Paper zDepth={1} id="SearchBar">
                <SearchField
                    autoFocus
                    id="searchField"
                    onKeyPress={this._handleEnter}
                    name="keyword"
                    value={this.state.keyword}
                    onChange={this._handleChange}
                    type="search"
                    style={
                        {
                            fontFamily: 'main'
                        }
                    }
                    floatingLabelText="아침은 먹었어요?"
                    floatingLabelStyle={
                        {
                            color: 'white',
                            width: '100%',
                            textAlign: 'center'
                        }
                    }
                    hintText="예: 경북대학교, 동성로 ..."
                    hintStyle={
                        {
                            color: 'white',
                            textAlign: 'center',
                            width: '100%'
                        }
                    }
                    inputStyle={
                        {
                            color: 'white',
                            textAlign: 'center'
                        }
                    }
                    fullWidth={true}
                />
                <Link to="/result" id="submitButton" onClick={() => {
                    this.props.cb(this.state.keyword);
                    localStorage.keyword = this.state.keyword
                }}>
                    <Button
                        label="검색"
                        fullWidth={true}
                        labelStyle={{fontFamily: 'main', color: 'white'}}
                        backgroundColor="black"
                    />
                </Link>
            </Paper>
        );
    }
}


export default SearchBar;