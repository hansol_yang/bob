import React from 'react';
import {ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';


const List = ({data}) => (
    <div>
        <ListItem
            primaryText={data.BZ_NM}
            secondaryText={data.SMPL_DESC}
            style={{fontFamily: 'main'}}
            primaryTogglesNestedList={true}
            nestedItems={
                [
                    (
                        <ListItem
                            key="0"
                            primaryText={"주변 정류장"}
                            secondaryText={data.BUS}
                            style={{fontFamily: 'main'}}
                        />
                    ),
                    (
                        <ListItem
                            key="1"
                            primaryText={"음식 분류"}
                            secondaryText={data.FD_CS}
                            style={{fontFamily: 'main'}}
                        />
                    ),
                    (
                        <ListItem
                            key="2"
                            primaryText={"영업 시간"}
                            secondaryText={data.MBZ_HR}
                            style={{fontFamily: 'main'}}
                        />
                    )
                ]
            }
        />
        <Divider/>
    </div>
);


export default List;