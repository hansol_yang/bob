import React from 'react';
import {connect} from 'react-redux';
import {List as MuiList} from 'material-ui/List';

import {searchRequest} from '../../redux/actions/search';
import List from '../components/Result/List';
import NavBar from '../components/common/NavBar';


class Result extends React.Component {

    componentDidMount() {
        this.props.searchRequest(localStorage.keyword);
    }

    render() {
        return (
            <div id="ResultContainer">
                <NavBar cb={this.props.searchRequest}/>
                <section className="list">
                    <MuiList style={{fontFamily: 'main'}}>
                        {this.props.result.length === 0 ? <div style={{padding: '1rem'}}>검색 결과가 없습니다.</div> :
                            this.props.result.map((bz, i) => (
                                <List
                                    data={bz}
                                    key={i}
                                />
                            ))}
                    </MuiList>
                </section>
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        result: state.searchReducer.result
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        searchRequest: (keyword) => dispatch(searchRequest(keyword))
    }
};

Result = connect(mapStateToProps, mapDispatchToProps)(Result);


export default Result;