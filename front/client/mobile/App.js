import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import Home from "./Home";
import Result from "./Result";


class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={Home}/>
                    <Route path="/result" component={Result}/>
                </div>
            </Router>
        );
    }
}


export default App;