import React from 'react';
import {connect} from 'react-redux';

import SearchBar from '../components/Home/SearchBar';
import {searchRequest} from '../../redux/actions/search';


class Home extends React.Component {
    render() {
        return (
            <div id="HomeContainer">
                <section className="background"></section>
                <p id="logo">
                    아먹남녀
                </p>
                <SearchBar cb={this.props.searchRequest}/>
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        // result: state.searchReducer.result
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        searchRequest: (keyword) => dispatch(searchRequest(keyword))
    }
};

Home = connect(mapStateToProps, mapDispatchToProps)(Home);


export default Home;