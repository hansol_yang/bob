import axios from 'axios';

import {SEARCH, SEARCH_SUCCESS, SEARCH_FAILURE} from "./ActionTypes";


export function search() {
    return {
        type: SEARCH
    }
}

export function searchSuccess(result) {
    return {
        type: SEARCH_SUCCESS,
        result
    }
}

export function searchFailure(err) {
    return {
        type: SEARCH_FAILURE,
        err
    }
}

export function searchRequest(keyword) {
    return function(dispatch) {
        dispatch(search());

        return axios.post('/search', {keyword})
            .then(function(res) {
                return dispatch(searchSuccess(res.data.data));
            })
            .catch(function(err) {
                return dispatch(searchFailure(err));
            })
    }
}