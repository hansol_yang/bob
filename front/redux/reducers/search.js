import {SEARCH, SEARCH_SUCCESS, SEARCH_FAILURE} from '../actions/ActionTypes';
import update from 'react-addons-update';


const initialState = {
    status: 'NONE',
    result: [],
};


const search = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH :
            return Object.assign({}, state, {
                status: 'WAIT'
            });

        case SEARCH_SUCCESS:
            return Object.assign({}, state, {
                status: 'SUCCESS',
                result: update(state.result, {
                    $set: action.result
                })
            });

        case SEARCH_FAILURE:
            return Object.assign({}, state, {
                status: 'FAILURE'
            });

        default:
            return state;
    }
};


export default search;