import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEvent from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';

import App from './client/mobile/App';
import reducer from './redux/reducers';

import '../back/public/stylesheets/style.css';
import '../back/public/stylesheets/home.css';
import '../back/public/stylesheets/result.css';
import '../back/public/stylesheets/detail.css';


injectTapEvent();

const rootElement = document.getElementById('root');

const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider>
            <App/>
        </MuiThemeProvider>
    </Provider>,
    rootElement
);

if(module.hot) {
    module.hot.accept();
}