var express = require('express');
var axios = require('axios');


var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index');
});

/* 검색어 서치 API */
router.post('/search', function (req, res, next) {
    if (req.body.keyword !== '') {
        axios({
            method: 'get',
            baseURL: encodeURI(`https://maps.googleapis.com/maps/api/geocode/json?address=${req.body.keyword}&language=ko&key=AIzaSyA4zZrnJA3ak88ddBXwbtYHmlIGYWT_-ik`)
        }).then(function (response) {
            var isDaegu = response.data.results[0].formatted_address.split(' ').indexOf('대구광역시');
            var searchGooGoon = response.data.results[0].formatted_address.split('시 ')[1].split(' ')[0];

            function isMatch(business) {
                return business.GNG_CS === searchGooGoon;
            }

            if (isDaegu !== -1) {
                axios.get('http://data.daegu.go.kr/hub/foodbrft?Key=9846775bc21246f08e0b5287de512660&Type=json&pIndex=1&pSize=46')
                    .then(function (response) {
                        return res.json({
                            data: response.data.foodbrft[1].row.filter(isMatch)
                        })
                    }).catch(function (err) {
                    console.error(err);

                });
            }
            else {
                return res.json({
                    data: []
                })
            }
        }).catch(function (err) {
            console.error(err);
        })
    } else {
        return res.json({
            data: []
        })
    }
});

module.exports = router;
